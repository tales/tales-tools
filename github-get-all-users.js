var token = '<snip>';
var repo = 'tales/tales-website';

function wait(x) {
    return new Promise(resolve => setTimeout(() => resolve(x), 76));
}

function getAllPages(url, arr = [], page = 1) {
    return fetch(`${url}&page=${page}`)
    .then(response => response.json())
    .then(wait)
    .then(res => {
        Array.prototype.push.apply(arr, res);
        if (res.length == 30) {
            return getAllPages(url, arr, page + 1);
        }
        return arr;
    })
}

function get(type) {
    return getAllPages(`https://api.github.com/repos/${repo}/${type}?access_token=${token}&state=all`);
};

function getComments(type, id) {
    return getAllPages(`https://api.github.com/repos/${repo}/${type}/${id}/comments?access_token=${token}`)
}

async function getAllMails() {
    var issues = await get("issues");
    var pulls = await get("pulls");
    
    var users = [];
    
    Array.prototype.push.apply(users, issues.map(issue => issue.user.login));
    Array.prototype.push.apply(users, pulls.map(pr => pr.user.login));

    let comments = await Promise.all(issues.map(issue => getComments('issues', issue.number)));
    comments.forEach(thread => Array.prototype.push.apply(users, thread.map(comment => comment.user.login)));

    let prComments = await Promise.all(pulls.map(pr => getComments('pulls', pr.number)));
    prComments.forEach(thread => Array.prototype.push.apply(users, thread.map(comment => comment.user.login)));
    
    let uniqueUsers = Array.from(new Set(users));
    let userInfos = await Promise.all(
        uniqueUsers.map(user => fetch(`https://api.github.com/users/${user}?access_token=${token}`)
            .then(wait)
            .then(response => response.json())
        )
    );
    console.log(userInfos.map(info => {return {login: info.login, mail: info.email};}));
}
getAllMails();